#include <iostream>

#include <to_the_power.h> 

int main()
{
    double number = 20;

    int power = 30;

    double result = to_the_power(number, power);

    std::cout << "Результат возведения числа "
              << number << " в "<< power << "-ю степень: "
              << result << std::endl;

    return 0;
}