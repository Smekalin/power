#include <iostream>
#include <stdint.h>

#include <to_the_power.h>

double to_the_power(double number, int power)
{
    double res = 1;

    double current = number;

    while(power>0)
    {
        if (power&1 == 1)
        {
            res *= current;
        }

        current *= current;

        power = power>>1;
    }
    return res;
}